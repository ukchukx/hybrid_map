package com.hybridmap;

import java.awt.Dimension;
import java.awt.Toolkit;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.beans.value.ObservableValue;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.ToolBar;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.util.Duration;

public class HybridMap extends Application {
    private Timeline delay;

    @Override 
    public void start(Stage primaryStage) {
        
        WebView webView = new WebView();
        WebEngine webEngine = webView.getEngine();
        webEngine.load(getClass().getResource("google.html").toString());
        
        
        ToggleGroup mapTypeGroup = new ToggleGroup();
        ToggleButton roadBtn = new ToggleButton("Road"),
        		satelliteBtn = new ToggleButton("Satellite"),
        				hybridBtn = new ToggleButton("Hybrid"),
        				terrainBtn = new ToggleButton("Terrain");
        roadBtn.setSelected(true);
        roadBtn.setToggleGroup(mapTypeGroup);
        satelliteBtn.setToggleGroup(mapTypeGroup);
        hybridBtn.setToggleGroup(mapTypeGroup);
        terrainBtn.setToggleGroup(mapTypeGroup);
        mapTypeGroup.selectedToggleProperty().addListener(
          (ObservableValue<? extends Toggle> observableValue, Toggle oldToggle, Toggle newToggle) -> {
            if (roadBtn.isSelected()) webEngine.executeScript("document.setMapType(\"Road\")");
            else if (satelliteBtn.isSelected()) webEngine.executeScript("document.setMapType(\"Satellite\")");
            else if (hybridBtn.isSelected()) webEngine.executeScript("document.setMapType(\"Hybrid\")");
            else if (terrainBtn.isSelected()) webEngine.executeScript("document.setMapType(\"Terrain\")");
          }
        );
        
        
        ToggleGroup mapGroup = new ToggleGroup();
        ToggleButton googleBtn = new ToggleButton("Google"),
        		bingBtn = new ToggleButton("Bing");
        googleBtn.setSelected(true);
        googleBtn.setToggleGroup(mapGroup);
        bingBtn.setToggleGroup(mapGroup);
        
        mapGroup.selectedToggleProperty().addListener((ObservableValue<? extends Toggle> observableValue, Toggle oldToggle, Toggle newToggle) -> {
          terrainBtn.setDisable(true);
          if (googleBtn.isSelected()) {
            terrainBtn.setDisable(false);
            webEngine.load(getClass().getResource("google.html").toString());
          } 
          else if (bingBtn.isSelected()) webEngine.load(getClass().getResource("bing.html").toString());
         
          mapTypeGroup.selectToggle(roadBtn);
        });
        
        TextField searchBox = new TextField("4.7500� N, 7.0000� E");
        searchBox.setPromptText("Address,Location or Co-ordinates");
        searchBox.textProperty().addListener((ObservableValue<? extends String> observableValue, String oldString, String newString) -> {
          // delay location updates so we don't type too fast
          if (delay!=null) delay.stop();
          delay = new Timeline();
          delay.getKeyFrames().add(
                  new KeyFrame(new Duration(300), (e) -> 
                    webEngine.executeScript("document.goToLocation(\""+searchBox.getText()+"\")"))
          );
          delay.play();
        });
        
        Button zoomInBtn = new Button("Zoom In");
        zoomInBtn.setOnAction((e) -> webEngine.executeScript("document.zoomIn()"));
        Button zoomOutBtn = new Button("Zoom Out");
        zoomOutBtn.setOnAction((e) -> webEngine.executeScript("document.zoomOut()"));
        
        ToolBar toolBar = new ToolBar();
        toolBar.getStyleClass().add("map-toolbar");
        toolBar.getItems().addAll(
                roadBtn, satelliteBtn, hybridBtn, terrainBtn,
                createSpacer(),
                googleBtn, bingBtn,
                createSpacer(),
                new Label("Location:"), searchBox, zoomInBtn, zoomOutBtn);
        
        BorderPane rootPane = new BorderPane();
        rootPane.getStyleClass().add("map");
        rootPane.setCenter(webView);
        rootPane.setTop(toolBar);
        
        primaryStage.setTitle("HybridMap");
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        
        Scene scene = new Scene(rootPane,screenSize.width-100,screenSize.height-100, Color.web("#666970"));
        primaryStage.setScene(scene);
        scene.getStylesheets().add(getClass().getResource("HybridMap.css").toExternalForm());
	    primaryStage.getIcons().add(new Image(getClass().getResource("map.png").toString()));
        
        primaryStage.show();
    }

    private Node createSpacer() {
        Region spacer = new Region();
        HBox.setHgrow(spacer, Priority.ALWAYS);
        return spacer;
    }
    
    public static void main(String[] args){
      System.setProperty("java.net.useSystemProxies", "true");
      launch(args);
    }
}